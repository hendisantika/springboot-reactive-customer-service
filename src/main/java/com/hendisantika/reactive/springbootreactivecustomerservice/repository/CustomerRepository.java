package com.hendisantika.reactive.springbootreactivecustomerservice.repository;

import com.hendisantika.reactive.springbootreactivecustomerservice.entity.Customer;
import org.bson.types.ObjectId;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-reactive-customer-service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-17
 * Time: 08:42
 */
public interface CustomerRepository extends ReactiveCrudRepository<Customer, ObjectId> {
}
