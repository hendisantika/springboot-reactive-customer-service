package com.hendisantika.reactive.springbootreactivecustomerservice.entity.enums;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-reactive-customer-service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-17
 * Time: 08:39
 */
public enum PhoneType {
    HOME, CELLULAR, OFFICE, FAX
}
