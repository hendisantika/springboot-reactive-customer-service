package com.hendisantika.reactive.springbootreactivecustomerservice.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-reactive-customer-service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-17
 * Time: 08:35
 */
@RestControllerAdvice
public class CustomerServiceExceptionHandler {

    @ExceptionHandler(CustomerServiceException.class)
    public ResponseEntity<?> handleControllerException(CustomerServiceException ex) {
        return new ResponseEntity<>(ex.getMessage(), ex.getHttpStatus());
    }

    @ExceptionHandler({AccessDeniedException.class})
    public ResponseEntity<?> handleAccessDeniedException(Exception ex) {
        return new ResponseEntity<>(ex.getMessage(), FORBIDDEN);
    }

    @ExceptionHandler(Exception.class)
    ResponseEntity<?> handleControllerException(Exception ex) {
        return new ResponseEntity<>(ex.getMessage(), INTERNAL_SERVER_ERROR);
    }

}