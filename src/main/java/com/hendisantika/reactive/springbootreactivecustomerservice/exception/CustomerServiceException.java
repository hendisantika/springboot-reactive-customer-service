package com.hendisantika.reactive.springbootreactivecustomerservice.exception;

import org.springframework.http.HttpStatus;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-reactive-customer-service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-17
 * Time: 08:34
 */
public class CustomerServiceException extends RuntimeException {

    private static final long serialVersionUID = 8631466448974323851L;

    private final HttpStatus httpStatus;
    private final String message;

    public CustomerServiceException(HttpStatus httpStatus, String message) {
        this.httpStatus = httpStatus;
        this.message = message;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public String getMessage() {
        return message;
    }
}