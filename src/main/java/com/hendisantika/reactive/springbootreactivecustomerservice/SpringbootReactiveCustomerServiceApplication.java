package com.hendisantika.reactive.springbootreactivecustomerservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;

@SpringBootApplication
@EnableReactiveMongoRepositories
public class SpringbootReactiveCustomerServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootReactiveCustomerServiceApplication.class, args);
    }

}

