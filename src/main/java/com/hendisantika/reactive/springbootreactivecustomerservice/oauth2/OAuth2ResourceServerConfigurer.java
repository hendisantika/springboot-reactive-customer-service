package com.hendisantika.reactive.springbootreactivecustomerservice.oauth2;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-reactive-customer-service
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-17
 * Time: 09:07
 */
@EnableResourceServer
@Configuration
public class OAuth2ResourceServerConfigurer extends ResourceServerConfigurerAdapter {
}